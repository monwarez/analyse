/*
Copyright (c) 2016, Payet Thibault
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Monwarez Inc nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL PAYET THIBAULT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "schema.h"
#include <iostream>

Schema::Schema(double K, unsigned int J, double theta, double finalTime)
{
    m_K            =    K;
    m_J            =    J;
    m_theta        =    theta;
    m_finalTime    =    finalTime;
    m_U            =    Eigen::VectorXd(J);
    m_A            =    SpMat(J,J);
    std::vector<Eigen::Triplet<double>> coeff(3*J -2);
    for (auto i = 0; i < J; ++i)
    {
        coeff[i] = Eigen::Triplet<double>(i,i,-2.0);
    }
    for (unsigned int i=1; i < J; ++i)
    {
        coeff[i+J-1] = Eigen::Triplet<double>(i-1,i,-1.0);
        coeff[i+2*J-2] = Eigen::Triplet<double>(i,i-1,-1.0);
    }
    m_A.setFromTriplets(coeff.begin(), coeff.end());

}
void Schema::ThetaSchema(){};
void Schema::ShowA()
{
    std::cout << m_A << '\n';
}
double	Schema::Function(double t, double x)
{
	return 0.0;
}
